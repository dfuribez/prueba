#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  5 10:42:39 2018

@author: once
"""

import serial
import time

angulo = 0
with serial.Serial("/dev/ttyUSB0", 115200) as ser:
    while 1:
        angulo = input("angulo: ")
        if angulo == "u":
            ser.write(b"Hola\r")
            data = ser.readline().strip()
            print(data)
        ser.write("s{0:0>3}\n".format(angulo).encode())
