# dsPIC
Está trabajando a una frecuencia de instrucción de 20MHz el UART1 implementa la comunicación con la Raspberry -baudrate-

# Comandos

Los comandos deben tener menos de 20 carácteres -longitud modificable- y están delimitados por un sigo de "nueva línea" \n o por uno nulo "\0"

# Lista de comandos

<ul>
    <li> Acá colocaría mi comando.</li>
    <li>Si tan sólo tuviera uno</li>
</ul>

| Comando | Descipción |
| :-----: | :--------- |
| Axxx    | Mueve el servo $`\#1`$ a la posición xxx grados [^2]|
| Bxxx    | Mueve el servo $`\#2`$ a la posición xxx grados [^2]|
| U1      | Lanza la medida con el ultrasonido $`\#1`$ |



# Ultrasonido
Usa una interrupción externa (INT0) y el TIMER1 para calcular el tiempo de vuelo. El tiempo se mide en nanosegundos (ciclos de instrucción)y por defecto el timer tiene un prescaler de 1:8. La distancia en cm está dada por:

```math
d[cm] = \frac{t_{vuelo}}{2} * prescaler * 340 \frac{m}{s} * 100 \frac{cm}{m}
```

y el tiempo de vuelo está dado por:

```math
t_{vuelo}[s] = TMRx * T_{cy}
```

# PWM

Para el duty, el registro toma los valores 2000 para 0grados y 6300 para 180 grados. (Empíricamente, Teóricamente los extremos deberían ser 2500 y 5000) por lo que para calcular el duty correspondiente a un determinado ángulo se usa la figuiente formula:

```math
duty = \theta * 24 + 2000
```

# Timers

| Timer | Función | Bits | Prescaler |
| :---: | :------ | :--: | :-------: |
| TMR1  | Dedicado para calcular el tiempo de vuelo para el ultrasonido $`\#1`$ | 16 | 1:8 |
| TMR2 | Dedicado para generar el PWM para el servo $`\#1`$ | 32 | 1:8 |
| TMR3 | Dedicado para generar el PWM para el servo $`\#2`$ | 32 | 1:8 | 32 | 1:8 |
| TMR4 | N/A | 32 | N/A |
| TMR5 | N/A | 32 | N/A |

# Interrupciones externas
| Interrupción | Función |
| :----------: | :------ |
| INT0 | Ultrasonido $`\#1`$ |

# Pines
| Pin | Módulo | Función | E/S |
| :-: | :----: | :------ | :-: |
| 1   | N/A    | MCLR    | E   |
| 19  | RD3    | Trigger para el ultrasonido $`\#1`$ | S |
| 23  | INT1   | Lee el pulso generado por el ultrasonido $`\#1`$| E|
| 25  | U1TX   | Transmisión serial con la Raspberry | S|
| 26  | U1RX   | Recepción serial con la Raspberry | E |
| 33  | OC2    | PWM para el servo $`\#2`$| S|
| 34  | OC1    | PWM para el servo $`\#1`$| S|



[^2]: El ángulo <b>siempre</b> debe estar dado por tres números; por ejemplo, si se quieren mover el servo $`\#2`$ a 10 grados, el comando coreespondiente es B<b>0</b>10.