
#if defined(__XC16__)
    #include <xc.h>
#elif defined(__C30__)
    #if defined(__dsPIC30F__)
        #include <p30Fxxxx.h>
    #endif
#endif

#include <stdint.h>          /* For uint32_t definition                       */
#include <stdbool.h>         /* For true/false definition                     */

#include "user.h"            /* variables/params used by user.c               */

void InitApp(void) {
    /* Setup analog functionality and port direction */
    TRISA       = 0x0000;
    PORTA       = 0x0000;
    LATA        = 0x0000;
   
    TRISB       = 0x0000;
    PORTB       = 0x0000;
    LATB        = 0x0000;
    
    TRISC       = 0x0000;
    PORTC       = 0x0000;
    LATC        = 0x0000;
    
    TRISD       = 0x0000;
    
    TRISDbits.TRISD8    = 0b1;
    //TRISDbits.TRISD0    = 0;
    
    PORTD       = 0x0000;
    LATC        = 0x0000;
    
    TRISF       = 0x0000;
    PORTF       = 0x0000;
    LATF        = 0x0000;
    
    ADPCFG      = 0xFFFF;
    
    /* Initialize peripherals */
    
    // SERIAL
    // UART1
    // 115200 bd
    
    U1MODEbits.PDSEL        = 0b00;     // 8bits sin paridad
    U1MODEbits.STSEL        = 0;        // 1 bit de parada
    
    U1BRG                   = 10;       // bd 115200
        
    U1MODEbits.ALTIO        = 0;        // No usa los pines alternos
    U1MODEbits.UARTEN       = 1;        // Habilita el serial 1
    U1STAbits.UTXEN         = 1;        // Habilita la transmisi�n
    
    U1STAbits.URXISEL       = 0b00;     // Habilita la interrupci�n cuaNDO LLEGA UN BYTE COMPLETO
    IPC2bits.U1RXIP         = 7;        // M�xima prioridad interrupci�n


    // TIMER1
    // TMR1
    T1CONbits.TCKPS         = 0b01;     // 1:8
    T1CONbits.TCS           = 0;        // Reloj
    T1CONbits.TON           = 0;        // Deshabilitado por defecto

    IPC0bits.T1IP           = 0b110;    // Prioridad 6
    IEC0bits.T1IE           = 0;        // Habilita las interrupciones
    
    IEC0bits.U1RXIE         = 1;        // Habilitamos las interrupciones recepci�n
    
    // INT1
    
    INTCON2bits.INT1EP      = 1;        // Interrupci�n flanco sbida
    IPC4bits.INT1IP         = 7;        // Interrupci�n nivel 7
    IEC1bits.INT1IE         = 0;        // Habilitamos la interrupci�n

    // PWM - OC1
    // TMR2
    OC1R                    = 6300;     // dutty 5% 1ms 312
    OC1RS                   = 6300;     // dutty 5% 1ms 312
    OC1CONbits.OCM          = 0b110;    // Habilitamos PWM
    OC1CONbits.OCTSEL       = 0;        // Usa timer2
    PR2                     = 50000;    // Periodo de 20ms; frecuencia de 50Hz
    
    T2CONbits.TCKPS         = 0b01;     // 1:8
    T2CONbits.TON           = 0;        //
    
    // PWM - OC2
    // TMR3
    
    OC2R                    = 6300;
    OC2RS                   = 6300;
    OC2CONbits.OCTSEL       = 1;        // Usa el timer3
    OC2CONbits.OCM          = 0b110;    // Habilita el PWM
    PR3                     = 50000;
    
    T3CONbits.TCKPS         = 0b01;     // 1:8
    T3CONbits.TON           = 0;
}

