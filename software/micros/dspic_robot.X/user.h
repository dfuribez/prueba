void InitApp(void); /* I/O and Peripheral Initialization */

/*serial.c*/
void enviar(uint8_t *mensaje_ptr, uint16_t longitud);
void recibir(uint8_t *buffer_recibido_uart1_ptr, uint16_t byte_recibido);
void enviar_numero(uint16_t numero);
void enviar_flotante(float numero);

/*common.c*/
void delay_ns(uint16_t microseconds);

/*ultrasonido*/
float leer_ultrasonido();

/*pwm.c*/
void pwm_servo(uint8_t angulo, uint8_t servo);