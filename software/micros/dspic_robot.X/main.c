
#if defined(__XC16__)
    #include <xc.h>
#elif defined(__C30__)
    #if defined(__dsPIC30F__)
        #include <p30Fxxxx.h>
    #endif
#endif

#include <stdint.h>        /* Includes uint16_t definition                    */
#include <stdbool.h>       /* Includes true/false definition  */

#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "user.h"          /* User funct/params, such as InitApp              */
#include "definiciones.c"   

#include <libpic30.h>


uint8_t     mensaje_UART1[LEN_MSG_UART1];   // Almacena el mensaje que llega por el UART1
uint8_t     buffer_UART1[LEN_MSG_UART1];    // almacena el mensaje a enviar por el UART1

uint16_t byte_UART1;        // Almacena el byte que llega por el UART1
uint16_t tiempo_vuelo;

/* DECLARACI�N DE LAS BANDERAS */
uint8_t r1_flag = 0;  // Bandera recepci�n nuevo caracter por UART1
uint8_t u1_flag = 0;  // Bandera que indica que se est� midiendo pulso del ultrasonido

uint8_t inicio = 0;
        
int16_t main(void) {
    ConfigureOscillator();
    InitApp();

    enviar("Bienvenido!\n", LEN_MSG_UART1);
    
    while(1) {
        if (r1_flag) {
            //LATDbits.LATD2 = ~PORTDbits.RD2;
            recibir(&mensaje_UART1, byte_UART1);
            r1_flag = 0;
        } // end-if recibido serial     
        
    }
} 