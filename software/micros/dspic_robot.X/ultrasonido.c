
#include <p30F4013.h>

#include "definiciones.c"

#include <libpic30.h>

extern uint8_t u1_flag;
extern uint16_t tiempo_vuelo;

float leer_ultrasonido() {
    // Configuramos la interrupción por flanco de subida
    INTCON2bits.INT1EP = 0;     // Interrupción en el flanco de subida
    IEC0bits.U1RXIE = 0;  // Deshabilita las interrupciones en la recepcion del serial
    
    // Lanzamos el pulso
    LATDbits.LATD3 = 0;
    __delay_ms(5);
    LATDbits.LATD3 = 1;
    __delay_ms(20);
    LATDbits.LATD3 = 0;
    IEC1bits.INT1IE    = 1;    
    // Esperamos que termine la medición
    while (u1_flag != 3);
    u1_flag = 0;
    IEC0bits.U1RXIE = 1;  // Habilita interrupción recepción serial
    //return tiempo_vuelo * 0.0544;  // prescaler 64
    return tiempo_vuelo * 0.0068;  // prescaler 8
    //return tiempo_vuelo;
    
    
}