
#include <p30F4013.h>
#include <string.h>
#include <stdio.h>
#include <xc.h>

#include "definiciones.c"
#include "user.h"

extern uint8_t buffer_UART1[LEN_MSG_UART1];


uint8_t posicion_mensaje = 0;



/**
 * 
 * @param buffer_recibido_uart1_ptr Puntero al array donde se almacenar� el mensaje recibido
 * @param byte_recibido Contiene el byte recibido por el serial
 */
void recibir(uint8_t *buffer_recibido_uart1_ptr, uint16_t byte_recibido) {
    
    if (byte_recibido == 0 || posicion_mensaje == LEN_MSG_UART1 || byte_recibido == 10 || byte_recibido == 13) {
        // Limpiamos las posiciones restantes del array
        for (uint8_t pos = posicion_mensaje; pos < LEN_MSG_UART1; pos++) {
            *(buffer_recibido_uart1_ptr + pos) = 0;  // Llenamos las posiciones restantes del mensaje con car�cteres nulos
        } // end-for
        posicion_mensaje = 0;
        //enviar("Recibido\n", 100);
        parsear(buffer_recibido_uart1_ptr);
    } else {
        *(buffer_recibido_uart1_ptr + posicion_mensaje) = byte_recibido;
        posicion_mensaje++;
    } // end-if
    
} //end-function recibir

/**
 * 
 * @param mensaje_ptr puntero al array donde est� el mensaje a enviar
 * @param longitud longitud del mensaje a enviar
 * 
 * El envio para cuando se encuentra con un car�cter nulo o llega
 * a la longitud del mensaje
 */

void enviar(uint8_t *mensaje_ptr, uint16_t longitud) {
    uint16_t posicion = 0;
    uint8_t byte = 0;
    
    while (posicion < longitud) {
        if (U1STAbits.TRMT) {
            byte = *(mensaje_ptr + posicion);
            
            // Detiene el envio cuando se encuentra un salto de linea
            // o un caracter vac�o
            if (byte == 10 || byte == 0) {
                U1TXREG = 10;  // ASCII para el salto de l�nea
                return;
            } else {
                U1TXREG = byte;
                posicion++;
            } //end-if
            
        } // end-if
    } // end-while
} // end-function enviar


/**
 * 
 * @param mensaje_ptr Mensaje con el comando a parsear
 * 
 * Funci�n que parsea y ejecuta los comandos recibidos por el serial
 */

void parsear(uint8_t *mensaje_ptr) {
    uint16_t comparacion = 0;
    comparacion = strncmp(mensaje_ptr, "U1", 2);
    
    if (comparacion == 0) {
        LATDbits.LATD2 = 1;  // enciende un led para verficar que comando es correcto
        enviar_flotante(leer_ultrasonido());
    } else {
        LATDbits.LATD2 = 0;  // enciende un led para verficar que comando es correcto
    }
    
    comparacion = strncmp(mensaje_ptr, "A", 1);
    if (comparacion == 0) {
        uint8_t numero = (*(mensaje_ptr + 1) - 48) * 100 + (*(mensaje_ptr + 2) - 48) * 10 + (*(mensaje_ptr + 3) - 48);
        pwm_servo(numero, 1);
        LATDbits.LATD2 = 1;
    }
    
    comparacion = strncmp(mensaje_ptr, "B", 1);
    if (comparacion == 0) {
        uint8_t numero = (*(mensaje_ptr + 1) - 48) * 100 + (*(mensaje_ptr + 2) - 48) * 10 + (*(mensaje_ptr + 3) - 48);
        pwm_servo(numero, 2);
        LATDbits.LATD2 = 1;
    }    
}

void enviar_numero(uint16_t numero) {
    sprintf(buffer_UART1, "%d", numero);
    enviar(buffer_UART1, LEN_MSG_UART1);
}

void enviar_flotante(float numero) {
    sprintf(buffer_UART1, "%f", numero);
    enviar(buffer_UART1, LEN_MSG_UART1);
}