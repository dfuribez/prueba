#include <p30F4013.h>
#include <math.h>

uint16_t duty;

void pwm_servo(uint8_t angulo, uint8_t servo) {    
    if (angulo >= 0 && angulo <= 180) {
        if (servo == 1) {
            T2CONbits.TON   = 0;  // Apagamos el timer
        } else {
            T3CONbits.TON   = 0;  // Apagamos el timer
        }
        duty = (uint16_t) ((angulo * 24) + 2000);  // Calculamos el nuevo dutty
        
        if (servo == 1) {
            OC1R = duty;
            OC1RS = duty;
            T2CONbits.TON   = 1;  // Encendemos el timer
        } else {
            OC2R = duty;
            OC2RS = duty;
            T3CONbits.TON   = 1;  // Encendemos el timer
        }
    }
}